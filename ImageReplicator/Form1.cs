﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageReplicator
{
    public partial class Form1 : Form
    {
        private bool draw = false;
        private int dots = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            draw = true;
            stopButton.Enabled = true;
            StartButton.Enabled = false;
            loadButton.Enabled = false;

            Thread drawerThread = new Thread(() =>
            {
                dots = 0;
                Bitmap image = new Bitmap(pictureBoxOriginal.Image.Width, pictureBoxOriginal.Image.Height);
                Bitmap original = new Bitmap(pictureBoxOriginal.Image);
                Graphics graphics = Graphics.FromImage(image);

                Random rand = new Random();
                Bitmap old = (Bitmap)image.Clone();

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                while (draw)
                {
                    int x = (int)(rand.NextDouble() * image.Width);
                    int y = (int)(rand.NextDouble() * image.Height);

                    int s = (int)(rand.NextDouble() * Math.Min(image.Height * 0.05, image.Width * 0.05));

                    s = Math.Max(s, Math.Max(2, (int)Math.Min(image.Height * 0.02, image.Width * 0.02)));

                    Color color = Color.FromArgb(255, (byte)(rand.NextDouble() * 255), (byte)(rand.NextDouble() * 255), (byte)(rand.NextDouble() * 255));

                    graphics.DrawEllipse(new Pen(color, s), new Rectangle(x, y, s, s));

                    int xf = x - s;
                    int xt = x + s * 2;

                    int yf = y - s;
                    int yt = y + s * 2;

                    if (!IsNewImageBetter(original, old, image, xf, xt, yf, yt))
                    {
                        for (int xo = Math.Max(0, xf); xo < Math.Min(original.Width, xt); xo++)
                        {
                            for (int yo = Math.Max(0, yf); yo < Math.Min(original.Height, yt); yo++)
                            {
                                image.SetPixel(xo, yo, old.GetPixel(xo, yo));
                            }
                        }
                    }
                    else
                    {
                        old = (Bitmap)image.Clone();

                        dots++;

                        pictureBoxReplica.Invoke((MethodInvoker)delegate
                        {
                            if (WindowState != FormWindowState.Minimized)
                            {
                                dotsLabel.Text = "Dots: " + String.Format("{0:n0}", dots);

                                pictureBoxReplica.Image = image;
                                pictureBoxReplica.Refresh();
                            }
                        });
                    }

                    pictureBoxReplica.Invoke((MethodInvoker)delegate
                    {
                        timeLabel.Text = "Time: " + (int)stopWatch.Elapsed.TotalHours + "h " + stopWatch.Elapsed.Minutes + "m " + stopWatch.Elapsed.Seconds + "s";
                    });
                }

                pictureBoxReplica.Invoke((MethodInvoker)delegate
                {
                    stopWatch.Stop();
                    saveButton.Enabled = true;
                    stopButton.Enabled = false;
                    StartButton.Enabled = true;
                    loadButton.Enabled = true;
                });
            });

            saveButton.Enabled = false;

            drawerThread.Start();
        }

        private void pictureBoxReplica_Click(object sender, EventArgs e)
        {
            if(saveButton.Enabled)
            {
                saveButton_Click(sender, e);
            }
        }
        
        private void pictureBoxReplica_Paint(object sender, PaintEventArgs gr)
        {
            
        }
        
        private bool IsNewImageBetter(Bitmap original, Bitmap old, Bitmap replica, int xf, int xt, int yf, int yt)
        {
            if (original.Width != replica.Width || original.Height != replica.Height)
                throw new InvalidOperationException("The images are not the same size");

            if (original.Width != old.Width || original.Height != old.Height)
                throw new InvalidOperationException("The images are not the same size");
            
            double oldError = 0;
            double newError = 0;

            for (int x = Math.Max(0, xf); x < Math.Min(original.Width, xt); x++)
            {
                for (int y = Math.Max(0, yf); y < Math.Min(original.Height, yt); y++)
                {
                    Color pixel = original.GetPixel(x, y);
                    oldError += ColorDifference(pixel, old.GetPixel(x, y));
                    newError += ColorDifference(pixel, replica.GetPixel(x, y));
                }
            }
            
            return oldError > newError;
        }

        private double ColorDifference(Color c1, Color c2)
        {
            float r = c1.R - c2.R;
            float g = c1.G - c2.G;
            float b = c1.B - c2.B;
            float a = c1.A - c2.A;
            
            return (r * r) + (g * g) + (b * b) + (a * a);
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            DialogResult result = openFileDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            pictureBoxOriginal.Image = Image.FromFile(openFileDialog.FileName);

            StartButton.Enabled = true;

            largeImageWarning.Visible = pictureBoxOriginal.Image.Width * pictureBoxOriginal.Image.Height > 600 * 600;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "PNG Image|*.png|JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            DialogResult result = saveFileDialog.ShowDialog();

            if (result != DialogResult.OK)
                return;

            Stream stream = saveFileDialog.OpenFile();

            saveFileDialog.AddExtension = true;
            
            switch (saveFileDialog.FilterIndex)
            {
                case 1:
                    pictureBoxReplica.Image.Save(stream, ImageFormat.Png);
                    break;

                case 2:
                    pictureBoxReplica.Image.Save(stream, ImageFormat.Jpeg);
                    break;

                case 3:
                    pictureBoxReplica.Image.Save(stream, ImageFormat.Bmp);
                    break;

                case 4:
                    pictureBoxReplica.Image.Save(stream, ImageFormat.Gif);
                    break;
            }

            stream.Close();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            draw = false;
        }

        private void pictureBoxOriginal_Click(object sender, EventArgs e)
        {
            if(loadButton.Enabled)
            {
                loadButton_Click(sender, e);
            }
        }
    }
}
