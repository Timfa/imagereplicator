﻿namespace ImageReplicator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBoxReplica = new System.Windows.Forms.PictureBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.loadButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.pictureBoxOriginal = new System.Windows.Forms.PictureBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.stopButton = new System.Windows.Forms.Button();
            this.largeImageWarning = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.dotsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxReplica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOriginal)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxReplica
            // 
            this.pictureBoxReplica.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxReplica.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxReplica.BackgroundImage")));
            this.pictureBoxReplica.Location = new System.Drawing.Point(518, 12);
            this.pictureBoxReplica.Name = "pictureBoxReplica";
            this.pictureBoxReplica.Size = new System.Drawing.Size(500, 418);
            this.pictureBoxReplica.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxReplica.TabIndex = 0;
            this.pictureBoxReplica.TabStop = false;
            this.pictureBoxReplica.Click += new System.EventHandler(this.pictureBoxReplica_Click);
            this.pictureBoxReplica.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxReplica_Paint);
            // 
            // StartButton
            // 
            this.StartButton.Enabled = false;
            this.StartButton.Location = new System.Drawing.Point(437, 437);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(75, 23);
            this.StartButton.TabIndex = 2;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // loadButton
            // 
            this.loadButton.Location = new System.Drawing.Point(12, 437);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(110, 23);
            this.loadButton.TabIndex = 5;
            this.loadButton.Text = "Load Image";
            this.loadButton.UseVisualStyleBackColor = true;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(948, 437);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(70, 23);
            this.saveButton.TabIndex = 6;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // pictureBoxOriginal
            // 
            this.pictureBoxOriginal.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxOriginal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxOriginal.BackgroundImage")));
            this.pictureBoxOriginal.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxOriginal.Name = "pictureBoxOriginal";
            this.pictureBoxOriginal.Size = new System.Drawing.Size(500, 418);
            this.pictureBoxOriginal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxOriginal.TabIndex = 7;
            this.pictureBoxOriginal.TabStop = false;
            this.pictureBoxOriginal.Click += new System.EventHandler(this.pictureBoxOriginal_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "image.png";
            // 
            // stopButton
            // 
            this.stopButton.Enabled = false;
            this.stopButton.Location = new System.Drawing.Point(518, 437);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 8;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // largeImageWarning
            // 
            this.largeImageWarning.AutoSize = true;
            this.largeImageWarning.Location = new System.Drawing.Point(144, 442);
            this.largeImageWarning.Name = "largeImageWarning";
            this.largeImageWarning.Size = new System.Drawing.Size(266, 13);
            this.largeImageWarning.TabIndex = 9;
            this.largeImageWarning.Text = "Warning: Large images may take very long to replicate!";
            this.largeImageWarning.Visible = false;
            // 
            // timeLabel
            // 
            this.timeLabel.Location = new System.Drawing.Point(602, 433);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(340, 13);
            this.timeLabel.TabIndex = 10;
            this.timeLabel.Text = "Time: 0h 0m 0s";
            this.timeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dotsLabel
            // 
            this.dotsLabel.Location = new System.Drawing.Point(599, 446);
            this.dotsLabel.Name = "dotsLabel";
            this.dotsLabel.Size = new System.Drawing.Size(343, 23);
            this.dotsLabel.TabIndex = 11;
            this.dotsLabel.Text = "Dots: 0";
            this.dotsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 470);
            this.Controls.Add(this.dotsLabel);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.largeImageWarning);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.pictureBoxOriginal);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.pictureBoxReplica);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Image Replicator";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxReplica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOriginal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxReplica;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Button loadButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.PictureBox pictureBoxOriginal;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Label largeImageWarning;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label dotsLabel;
    }
}

